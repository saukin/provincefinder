/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.saukin.entities;

/**
 *
 * @author S_SAUKIN
 */
public class Province {
    
    private String name;
    private String code;

    public Province() {
        this.name = "";
        this.code = "";
    }

    public Province(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Province{" + "name=" + name + '}';
    }
    
    
    
    
}

