/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.saukin.province_fiinder;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import me.saukin.entities.Province;

/**
 *
 * @author S_SAUKIN
 */
@WebService(serviceName = "PFinder")
@Stateless()
public class PFinder {


    /**
     * Web service operation
     */
    @WebMethod(operationName = "operation")
    public String operation(@WebParam(name = "code") String code) {
        String prName = "";
        
        switch (code) {
            case "QC": prName = "Quebec";
                break;
            case "ON": prName = "Ontario";
                break;
            case "NS": prName = "Nova Scottia";
                break;
        }
        
        return prName;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getVersion")
    public Double getVersion() {
        return 1.0;
    }
    
    @WebMethod(operationName = "getProvinceObject")
    public Province getProvinceObject(String s) {
        Province p = new Province();
        p.setName(operation(s));
        p.setCode(s);
        return p;
    }
    
    
    
}
